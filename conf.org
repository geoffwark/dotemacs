#+TITLE: conf.org --- My personal GNU Emacs config
#+DATE: <2017-06-07 Wed>
#+AUTHOR: Geoffrey T. Wark
#+EMAIL: geoff@geoffwark.us

* First Things First

prevent customization settings from appending junk to this file

#+BEGIN_SRC emacs-lisp
  (setq custom-file "~/.emacs.d/custom.el")
  (load custom-file :noerror)
#+END_SRC

don't clutter dirs w/ temp files!

#+BEGIN_SRC emacs-lisp
  (setq backup-directory-alist
        `((".*" . ,temporary-file-directory)))
  (setq auto-save-file-name-transforms
        `((".*" ,temporary-file-directory t)))
#+END_SRC

Let Emacs know who I am.

#+BEGIN_SRC emacs-lisp
  (setq user-full-name "Geoffrey T. Wark"
        user-mail-address "geoff@geoffwark.us")
#+END_SRC

Always install packages automatically if not already present on the system.

#+BEGIN_SRC emacs-lisp
  (setq use-package-always-ensure t)
#+END_SRC

* UI/UX
** Free up screen real estate

The following toolbars take up unnecessary space:

#+BEGIN_SRC emacs-lisp
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
#+END_SRC

** Theme

[[http://kippura.org/zenburnpage/][Zenburn]] is pretty easy on the eyes.

#+BEGIN_SRC emacs-lisp
  (use-package zenburn-theme
    :init
    (load-theme 'zenburn t))
#+END_SRC

** Face

Italicize comments.

#+BEGIN_SRC emacs-lisp
  (make-face-italic 'font-lock-comment-face)
#+END_SRC

Display trailing whitespace by default.  I like to use the color orange for this as it is quite jarring.

#+BEGIN_SRC emacs-lisp
  (setq-default show-trailing-whitespace t)
  (set-face-background 'trailing-whitespace "orange")
#+END_SRC

Make 'region' colors more obvious.

#+BEGIN_SRC emacs-lisp
  (set-face-attribute 'region nil
                      :background "white smoke"
                      :foreground "black")
#+END_SRC

** Cursor

Can be hard to see.  I make it bright red so that it's easy to track.

#+BEGIN_SRC emacs-lisp
  (add-to-list 'default-frame-alist '(cursor-color . "red"))
#+END_SRC

Also stop that goddamn blinking!

#+BEGIN_SRC emacs-lisp
  (blink-cursor-mode -1)
#+END_SRC

** Font

[[https://pagure.io/liberation-fonts][Liberation Fonts]] are pretty dope.  Be sure you have them installed.

#+BEGIN_SRC emacs-lisp
  (add-to-list 'default-frame-alist '(font . "Liberation Mono-11"))
#+END_SRC

** General

Focus the current line.

#+BEGIN_SRC emacs-lisp
  (global-hl-line-mode t)
#+END_SRC

Display column numbers in the mode line.

#+BEGIN_SRC emacs-lisp
  (setq column-number-mode t)
#+END_SRC

* Indentation

I *HATE* tabs!  Turn them off! >:(

#+BEGIN_SRC emacs-lisp
  (setq-default indent-tabs-mode nil)
#+END_SRC

_NOTE:_ You can insert a tab if needed with =C-q tab=

** Default

Use 2 spaces per indent.

#+BEGIN_SRC emacs-lisp
  (setq-default tab-width 2)
#+END_SRC

* Sane Defaults

Always use UTF-8.  If you really need something else, use =C-x C-m f= to re-encode.

#+BEGIN_SRC emacs-lisp
  (setq locale-coding-system 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (set-selection-coding-system 'utf-8)
  (prefer-coding-system 'utf-8)
#+END_SRC

Auto revert file buffers when there have been changes on the disk.

#+BEGIN_SRC emacs-lisp
  (global-auto-revert-mode t)
#+END_SRC

Delete text in region if/when typed in (like a /normal/ editor).

#+BEGIN_SRC emacs-lisp
  (delete-selection-mode t)
#+END_SRC

Don't break lines for me, please

#+BEGIN_SRC emacs-lisp
  (setq-default truncate-lines t)
#+END_SRC

Move files to Trash when deleting.

#+BEGIN_SRC emacs-lisp
  (setq delete-by-moving-to-trash t)
#+END_SRC

Visually indicate empty lines after the buffer ends.

#+BEGIN_SRC emacs-lisp
  (setq-default indicate-empty-lines t)
#+END_SRC

Try to flash the frame to represent a bell.

#+BEGIN_SRC emacs-lisp
  (setq visible-bell t)
#+END_SRC

Allow minibuffer commands while in the minibuffer.

#+BEGIN_SRC emacs-lisp
  (setq enable-recursive-minibuffers t)
#+END_SRC

Easily navigate sillycased words.

#+BEGIN_SRC emacs-lisp
  (global-subword-mode t)
#+END_SRC

Show keystrokes in progress.

#+BEGIN_SRC emacs-lisp
  (setq echo-keystrokes 0.1)
#+END_SRC

Real emacs knights don't use shift to mark things

#+BEGIN_SRC emacs-lisp
  (setq shift-select-mode nil)
#+END_SRC

* Custom Functions

Insert and go to a newline from anywhere in the current line.

#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "M-n") '(lambda ()
                                 (interactive)
                                 (end-of-line)
                                 (newline-and-indent)))
#+END_SRC

Streamlink (useful for watching Twitch.tv)

#+BEGIN_SRC emacs-lisp
  (defun my/streamlink (link quality)
    (interactive "sLink: \nsQuality? ")
    (async-shell-command (concat "streamlink " link " " quality)))
#+END_SRC

[[https://www.emacswiki.org/emacs/MoveLine][Move Line]]

#+BEGIN_SRC emacs-lisp
  (defun move-line (n)
    "Move the current line up or down by N lines."
    (interactive "p")
    (setq col (current-column))
    (beginning-of-line) (setq start (point))
    (end-of-line) (forward-char) (setq end (point))
    (let ((line-text (delete-and-extract-region start end)))
      (forward-line n)
      (insert line-text)
      ;; restore point to original column in moved line
      (forward-line -1)
      (forward-char col)))

  (defun move-line-up (n)
    "Move the current line up by N lines."
    (interactive "p")
    (move-line (if (null n) -1 (- n))))

  (defun move-line-down (n)
    "Move the current line down by N lines."
    (interactive "p")
    (move-line (if (null n) 1 n)))

  (global-set-key (kbd "M-<up>") 'move-line-up)
  (global-set-key (kbd "M-<down>") 'move-line-down)
#+END_SRC

Automatically Create Parent Directories on Visiting a New File (credit to [[http://iqbalansari.me/blog/2014/12/07/automatically-create-parent-directories-on-visiting-a-new-file-in-emacs/][Iqbal Ansari]])

#+BEGIN_SRC emacs-lisp
  (defun my-create-non-existent-directory ()
        (let ((parent-directory (file-name-directory buffer-file-name)))
          (when (and (not (file-exists-p parent-directory))
                     (y-or-n-p (format "Directory `%s' does not exist! Create it?" parent-directory)))
            (make-directory parent-directory t))))

  (add-to-list 'find-file-not-found-functions #'my-create-non-existent-directory)
#+END_SRC

editing with root-privileges ([[https://emacs-fu.blogspot.com/2013/03/editing-with-root-privileges-once-more.html][credit]])

#+BEGIN_SRC emacs-lisp
  (defun djcb-find-file-as-root ()
    "Like `ido-find-file, but automatically edit the file with
  root-privileges (using tramp/sudo), if the file is not writable by
  user."
    (interactive)
    (let ((file (ido-read-file-name "Edit as root: ")))
      (unless (file-writable-p file)
        (setq file (concat "/sudo:root@localhost:" file)))
      (find-file file)))
  ;; or some other keybinding...
  (global-set-key (kbd "C-x F") 'djcb-find-file-as-root)
#+END_SRC

Rename file and buffer (credit to [[https://rejeep.github.io/emacs/elisp/2010/03/26/rename-file-and-buffer-in-emacs.html][Johan Andersson]])

#+BEGIN_SRC emacs-lisp
  (defun rename-this-buffer-and-file ()
    "Renames current buffer and file it is visiting."
    (interactive)
    (let ((name (buffer-name))
          (filename (buffer-file-name)))
      (if (not (and filename (file-exists-p filename)))
          (error "Buffer '%s' is not visiting a file!" name)
        (let ((new-name (read-file-name "New name: " filename)))
          (cond ((get-buffer new-name)
                 (error "A buffer named '%s' already exists!" new-name))
                (t
                 (rename-file filename new-name 1)
                 (rename-buffer new-name)
                 (set-visited-file-name new-name)
                 (set-buffer-modified-p nil)
                 (message "File '%s' successfully renamed to '%s'" name (file-name-nondirectory new-name))))))))

  (global-set-key (kbd "C-c r") 'rename-this-buffer-and-file)
#+END_SRC

* Packages

Most of these come from [[https://melpa.org/#/][MELPA]].  Those that do not I throw into =elisp/=.

#+BEGIN_SRC emacs-lisp
  (add-to-list 'load-path "~/.emacs.d/elisp/")
#+END_SRC

** 
** fill-column-indicator

Graphically indicate the fill column

#+BEGIN_SRC emacs-lisp
  (use-package fill-column-indicator
    :init
    (setq-default fill-column 80)
    (setq fci-rule-color "pink")
    (setq fci-rule-width 1)
    (add-hook 'prog-mode-hook 'fci-mode))
#+END_SRC

** pianobar.el /(MANUAL)/

Run Pandora as an inferior process in emacs by using pianobar 

#+BEGIN_SRC emacs-lisp
  (autoload 'pianobar "pianobar" nil t)
#+END_SRC

** 

* Finishing Touches

Maximize and split the frame at startup.

#+BEGIN_SRC emacs-lisp
  (defun my/max-n-split (&optional frame)
    "a bad workaround."
    (interactive)
    (with-selected-frame (or frame (selected-frame))
      (toggle-frame-maximized)
      (split-window-right)))

  ;; GUI
  (if window-system
      (my/max-n-split))
  ;; daemon
  (add-hook 'after-make-frame-functions 'my/max-n-split)
#+END_SRC

http://www.patorjk.com/software/taag/

#+BEGIN_SRC emacs-lisp
  (message "
         ███▄ ▄███▓ ██▓  ██████   ██████  ██▓ ▒█████   ███▄    █
        ▓██▒▀█▀ ██▒▓██▒▒██    ▒ ▒██    ▒ ▓██▒▒██▒  ██▒ ██ ▀█   █
        ▓██    ▓██░▒██▒░ ▓██▄   ░ ▓██▄   ▒██▒▒██░  ██▒▓██  ▀█ ██▒
        ▒██    ▒██ ░██░  ▒   ██▒  ▒   ██▒░██░▒██   ██░▓██▒  ▐▌██▒
        ▒██▒   ░██▒░██░▒██████▒▒▒██████▒▒░██░░ ████▓▒░▒██░   ▓██░
        ░ ▒░   ░  ░░▓  ▒ ▒▓▒ ▒ ░▒ ▒▓▒ ▒ ░░▓  ░ ▒░▒░▒░ ░ ▒░   ▒ ▒
        ░  ░      ░ ▒ ░░ ░▒  ░ ░░ ░▒  ░ ░ ▒ ░  ░ ▒ ▒░ ░ ░░   ░ ▒░
        ░      ░    ▒ ░░  ░  ░  ░  ░  ░   ▒ ░░ ░ ░ ▒     ░   ░ ░
               ░    ░        ░        ░   ░      ░ ░           ░

   ▄████▄   ▒█████   ███▄ ▄███▓ ██▓███   ██▓    ▓█████▄▄▄█████▓▓█████
  ▒██▀ ▀█  ▒██▒  ██▒▓██▒▀█▀ ██▒▓██░  ██▒▓██▒    ▓█   ▀▓  ██▒ ▓▒▓█   ▀
  ▒▓█    ▄ ▒██░  ██▒▓██    ▓██░▓██░ ██▓▒▒██░    ▒███  ▒ ▓██░ ▒░▒███
  ▒▓▓▄ ▄██▒▒██   ██░▒██    ▒██ ▒██▄█▓▒ ▒▒██░    ▒▓█  ▄░ ▓██▓ ░ ▒▓█  ▄
  ▒ ▓███▀ ░░ ████▓▒░▒██▒   ░██▒▒██▒ ░  ░░██████▒░▒████▒ ▒██▒ ░ ░▒████▒
  ░ ░▒ ▒  ░░ ▒░▒░▒░ ░ ▒░   ░  ░▒▓▒░ ░  ░░ ▒░▓  ░░░ ▒░ ░ ▒ ░░   ░░ ▒░ ░
    ░  ▒     ░ ▒ ▒░ ░  ░      ░░▒ ░     ░ ░ ▒  ░ ░ ░  ░   ░     ░ ░  ░
  ░        ░ ░ ░ ▒  ░      ░   ░░         ░ ░      ░    ░         ░
  ░ ░          ░ ░         ░                ░  ░   ░  ░           ░  ░
  ░
  ")
#+END_SRC
