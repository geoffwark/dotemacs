;;; init.el --- My personal GNU Emacs config

;; Do NOT run garbage collection during startup by setting the value super high.
;; This drastically improves `emacs-init-time'.
;; NOTE: We reset this value later.
(setq gc-cons-threshold 999999999)

;; Ignore default REGEXP checks of file names at startup.
;; This also drastically improves `emacs-init-time'.
;; NOTE: Some bogus, benign errors will be thrown due of this...
(let ((file-name-handler-alist nil))

  ;; Set up package.el for use with MELPA
  (require 'package)
  (setq package-enable-at-startup nil)
  (add-to-list 'package-archives
	       '("melpa" . "https://melpa.org/packages/"))
  (package-initialize)

  ;; Bootstrap use-package.  It will manage all other packages.
  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))

  ;; Reduce load time of use-package
  (eval-when-compile
    (require 'use-package))
  (require 'diminish) ; allows for easy removal of packages' modeline strings
  (require 'bind-key) ; simplifies how keybindings are set

  ;; Tangle & load the rest of the config
  (org-babel-load-file "~/.emacs.d/conf.org")

  ;; more private settings
  (if (file-exists-p "~/Dropbox/.private.org")
      (org-babel-load-file "~/Dropbox/.private.org")
    ))

;; Revert garbage collection behavior to a normal, more modern level
(run-with-idle-timer
 5 nil
 (lambda ()
   (setq gc-cons-threshold 2000000)))

;;; init.el ends here
